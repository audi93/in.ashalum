<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->

<html lang="en">
<!--<![endif]-->

<head profile="http://dublincore.org/documents/2008/08/04/dc-html/">
    <meta charset="utf-8">

    <title>Ash Alum Chemicals | Manufacturing Value</title>
    <meta name="description" content="Ash Alum Chemicals is India's One of the Best Manufacturer and Exporter of White Coal Briquettes and other Biomass Products">
    <meta name="author" content="Ashish Purandare">
    <meta name="keywords" content="Ash Alum,Ash, Aluminimum, Alum, Ash Alum Chemicals, AAC, White Coal, Biomass Briquette, White Coal Briquettes, Wood Pellets, ashalumchemicals Bhilai">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><!-- Dublin Core Starts Here -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.Title" content="Ash Alum Chemicals | Manufacturing Value">
    <meta name="DC.Creator" content="Ashish Purandare">
    <meta name="DC.Subject" content="White Coal Briquette Manufacturer">
    <meta name="DC.Description" content="Ash Alum Chemicals is India's One of the Best Manufacturer and Exporter of White Coal Briquettes and other Biomass Products">
    <meta name="DC.Publisher" content="Ash Alum Chemicals">
    <meta name="DC.Contributor" content="Aditya Purandare">
    <meta name="DC.Date" content="2013-03-03">
    <meta name="DC.Type" content="Text">
    <meta name="DC.Language" content="en">
    <meta name="DC.Relation" content="White Coal Briquette">
    <meta name="DC.Coverage" content="India"><!-- Dublin Core Ends Here -->
    <!-- style -->
    <link rel="stylesheet" type="text/css" media="all" href="css/style.css"><!-- style end -->
    <!-- the grid -->
    <link rel="stylesheet" type="text/css" media="all" href="css/skeleton.css">
    <link rel="stylesheet" type="text/css" media="all" href="css/media.css"><!-- the grid end -->
    <!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
    <!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" media="all" href="css/IE.css">
<![endif]-->
    <!-- extras -->

    <script type="text/javascript" src="js/modernizr.custom.js">
</script><!-- extras end -->
</head>

<body>
    <!-- screen loader -->

    <div class="screen-loader"></div><!-- screen loader end -->
    <!-- preload -->

    <div id="preload">
        <!-- preload status -->

        <div id="preload-status"></div><!-- preload status end -->
    </div><!-- preload end -->
    <!-- curtains -->

    <div id="curtains"></div><!-- curtains end -->
    <!-- preload content -->

    <div class="preload-content">
        <!-- home -->

        <div id="home"></div><!-- home end -->
    </div><!-- preload content end -->
    <!-- menu -->

    <div class="menu">
        <ul>
            <li><a id="fire-home" class="menu-state active" href="#">Home<span>Get started</span></a></li>

            <li><a id="fire-about" class="menu-state" href="#">About<span>Who we are</span></a></li>

            <li><a id="fire-services" class="menu-state" href="#">Products<span>What we do</span></a></li>

            <li><a id="fire-contact" class="menu-state" href="#">Contact<span>Get in touch</span></a></li>

            <li><a id="fire-contact" class="menu-state" href="#">Careers<span>Work For us</span></a></li>
        </ul>
    </div><!-- menu end -->
    <!-- menu mobile -->

    <div class="menu-mobile">
        <ul>
            <li>
                <ul>
                    <li class="lifting"><a id="fire-home-mobile" class="menu-state active" href="#">Home</a></li>

                    <li class="lifting"><a id="fire-about-mobile" class="menu-state" href="#">About</a></li>

                    <li class="lifting"><a id="fire-services-mobile" class="menu-state" href="#">Products</a></li>

                    <li class="lifting-first"><a id="fire-contact-mobile" class="menu-state" href="#">Contact</a></li>

                    <li class="lifting-first"><a id="fire-contact-mobile" class="menu-state" href="#">Careers</a></li>
                </ul><a class="menu-mobile-trigger" href="#">Menu</a>
            </li>
        </ul>
    </div><!-- menu mobile end -->
    <!-- upper page -->

    <div id="read-home" class="upper-page current">
        <!-- borders -->

        <div class="borders"></div><!-- borders end -->
        <!-- social icons -->

        <div class="social-icons-wrapper">
            <ul class="social-icons">
                <li><a href="http://twitter.com/ashalumofficial"><img src="images/social/twitter.png" alt="Twitter"></a></li>

                <li><a href="#"><img src="images/social/facebook.png" alt="Facebook"></a></li>

                <li><a href="https://plus.google.com/107839837341786140611"><img src="images/social/googleplus.png" alt="GooglePlus"></a></li>

                <li><a href="#"><img src="images/social/youtube.png" alt="YouTube"></a></li>

                <li><a href="http://in.linkedin.com/pub/ashish-purandare/54/900/11a"><img src="images/social/linkedin.png" alt="LinkedIn"></a></li>
            </ul>
        </div><!-- social icons end -->
        <!-- upper content -->

        <div class="upper-content">
            <!-- container -->

            <div class="container center">
                <!-- intro wrapper -->

                <div id="intro-wrapper">
                    <!-- intro top line column -->
                    <!-- eight columns -->

                    <div class="eight columns column">
                        <!-- intro top line -->

                        <div id="intro-top-line"></div><!-- intro top line end -->
                    </div><!-- eight columns end -->
                    <!-- intro top line column end -->
                    <!-- intro top column -->
                    <!-- eight columns -->

                    <div class="eight columns column">
                        <!-- intro top -->

                        <div id="intro-top">
                            AAC's
                        </div><!-- intro top end -->
                    </div><!-- eight columns end -->
                    <!-- intro top column end -->
                    <!-- intro title column -->
                    <!-- sixteen columns -->

                    <div class="sixteen columns">
                        <!-- intro title -->

                        <div id="intro-title">
                            Ash Alum
                        </div><!-- intro title end -->
                    </div><!-- sixteen columns end -->
                    <!-- intro title column end -->
                    <!-- intro bottom column -->
                    <!-- eight columns -->

                    <div class="eight columns column">
                        <!-- intro bottom -->

                        <div id="intro-bottom">
                            Manufacturing Value
                        </div><!-- intro bottom end -->
                    </div><!-- eight columns end -->
                    <!-- intro bottom column end -->
                    <!-- intro bottom line column -->
                    <!-- eight columns -->

                    <div class="eight columns column">
                        <!-- intro bottom line -->

                        <div id="intro-bottom-line"></div><!-- intro bottom line end -->
                    </div><!-- eight columns end -->
                    <!-- intro bottom line column end -->
                </div><!-- intro wrapper end -->
            </div><!-- container end -->
        </div><!-- upper content end -->
        <!-- launch countdown -->
        <!-- sixteen columns -->

        <div class="sixteen columns">
            <div id="countdown-wrapper">
                <div id="countdown-wrap">
                    <ul id="countdown">
                        <!-- days -->

                        <li>
                            <span class="days">00</span>

                            <p class="timeRefDays">days</p>
                        </li><!-- days end -->
                        <!-- hours -->

                        <li>
                            <span class="hours">00</span>

                            <p class="timeRefHours">hours</p>
                        </li><!-- hours end -->
                        <!-- minutes -->

                        <li>
                            <span class="minutes">00</span>

                            <p class="timeRefMinutes">minutes</p>
                        </li><!-- minutes end -->
                        <!-- seconds -->

                        <li>
                            <span class="seconds">00</span>

                            <p class="timeRefSeconds">seconds</p>
                        </li><!-- seconds end -->
                    </ul>
                </div>
            </div>
        </div><!-- sixteen columns end -->
        <!-- launch countdown end -->
        <!-- newsletter form -->

        <div id="subscribe-wrapper">
            <div id="newsletter">
                <div class="newsletter">
                    <form id="subscribe" method="post" action="http://www.11-76.com/themes/time/image-background/subscribe.php">
                        <label class="mail">EMAIL *</label> <input id="subscribeemail" name="subscribeemail" type="text" class="subscriberequiredField subscribeemail"> <input id="submit" type="submit" value="SUBSCRIBE">
                    </form>
                </div>
            </div>
        </div><!-- newsletter form end -->
    </div><!-- upper page end -->
    <!-- about -->

    <div id="about" class="lower-page">
        <!-- lower content -->

        <div class="lower-content">
            <!-- about intro -->

            <div class="about-intro">
                <h1>About</h1>
            </div><!-- about intro end -->
            <!-- container -->

            <div class="container center">
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider left top -->

                    <div class="divider-left-top"></div><!-- divider left top end -->
                </div><!-- sixteen columns end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider right top -->

                    <div class="divider-right-top"></div><!-- divider right top end -->
                </div><!-- sixteen columns end -->
                <!-- about full -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <p>Established in 2009, Ash Alum Chemicals has manufactured & delivered products of the highest quality to its customers at most competetive market prices. We manufacture Whitecoal Briquette and are Central India Best Manufacturer and Exporter of Biomass Briquettes maintaining High Consicteny and Customer Satisfaction. Recently awarded as <a href="#">Chattisgarh State's Greenest Maunfacturing Unit Award</a>.<br>
                    <br>
                    Led by the Visionary Partner Mr. Ashish Purandare, we are on a Journey to establish Best of Supply Networks around the Globe.</p>
                </div><!-- sixteen columns end -->
                <!-- about full end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider blank -->

                    <div class="divider-blank"></div><!-- divider blank end -->
                </div><!-- sixteen columns end -->
                <!-- about column 1 -->
                <!-- one third column -->

                <div class="one-third column">
                    <h2>Process Evaluation</h2><!-- effect 8 -->

                    <section class="set-8">
                        <div class="hi-icon-wrap hi-icon-effect-8">
                            <a href="#" class="hi-icon hi-icon-pencil">Process Evaluation</a>
                        </div>
                    </section><!-- effect 8 end -->

                    <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipiscing elit</a>. Sed risus purus, pellentesque a consequat a, accumsan vel libero.</p>
                </div><!-- one third column end -->
                <!-- about column 1 end -->
                <!-- about column 2 -->
                <!-- one third column -->

                <div class="one-third column">
                    <h2>Quality Results</h2><!-- effect 8 -->

                    <section class="set-8">
                        <div class="hi-icon-wrap hi-icon-effect-8">
                            <a href="#" class="hi-icon hi-icon-star">Quality Results</a>
                        </div>
                    </section><!-- effect 8 end -->

                    <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipiscing elit</a>. Sed risus purus, pellentesque a consequat a, accumsan vel libero.</p>
                </div><!-- one third column end -->
                <!-- about column 2 end -->
                <!-- about column 3 -->
                <!-- one third column -->

                <div class="one-third column">
                    <h2>Customer Satisfaction</h2><!-- effect 8 -->

                    <section class="set-8">
                        <div class="hi-icon-wrap hi-icon-effect-8">
                            <a href="#" class="hi-icon hi-icon-contract">Customer Satisfaction</a>
                        </div>
                    </section><!-- effect 8 end -->

                    <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipiscing elit</a>. Sed risus purus, pellentesque a consequat a, accumsan vel libero.</p>
                </div><!-- one third column end -->
                <!-- about column 3 end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider left bottom -->

                    <div class="divider-left-bottom"></div><!-- divider left bottom end -->
                </div><!-- sixteen columns end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider right bottom -->

                    <div class="divider-right-bottom"></div><!-- divider right bottom end -->
                </div><!-- sixteen columns end -->
            </div><!-- container end -->
            <!-- sixteen columns -->

            <div class="sixteen columns">
                <!-- divider fin -->

                <div class="divider-fin"></div><!-- divider fin end -->
            </div><!-- sixteen columns end -->
        </div><!-- lower content end -->
        <!-- closer -->

        <div class="fire-closer">
            <a id="fire-about-closer" class="menu-state" href="#"><img src="images/fire-closer.png" alt="Closer"></a>
        </div><!-- closer end -->
        <!-- sixteen columns -->

        <div class="sixteen columns">
            <!-- divider fin -->

            <div class="divider-fin"></div><!-- divider fin end -->
        </div><!-- sixteen columns end -->
    </div><!-- about end -->
    <!-- services -->

    <div id="services" class="lower-page">
        <!-- lower content -->

        <div class="lower-content">
            <!-- services intro -->

            <div class="services-intro">
                <h1>Products</h1>
            </div><!-- services intro end -->
            <!-- container -->

            <div class="container center">
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider left top -->

                    <div class="divider-left-top"></div><!-- divider left top end -->
                </div><!-- sixteen columns end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider right top -->

                    <div class="divider-right-top"></div><!-- divider right top end -->
                </div><!-- sixteen columns end -->
                <!-- services full -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipiscing elit</a>. Donec placerat congue leo, nec vehicula elit vehicula vel. Duis odio nibh, malesuada sed luctus ut, consectetur vel dui. Mauris vel imperdiet nulla. Sed eu ligula augue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed a justo nec elit feugiat facilisis quis quis elit.</p>
                </div><!-- sixteen columns end -->
                <!-- services full end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider blank -->

                    <div class="divider-blank"></div><!-- divider blank end -->
                </div><!-- sixteen columns end -->
                <!-- services column 1 -->
                <!-- one third column -->

                <div class="one-third column">
                    <h2>Mobile Friendly</h2><!-- effect 8 -->

                    <section class="set-8">
                        <div class="hi-icon-wrap hi-icon-effect-8">
                            <a href="#" class="hi-icon hi-icon-mobile">Mobile Friendly</a>
                        </div>
                    </section><!-- effect 8 end -->

                    <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipiscing elit</a>. Sed risus purus, pellentesque a consequat a, accumsan vel libero.</p>
                </div><!-- one third column end -->
                <!-- services column 1 end -->
                <!-- services column 2 -->
                <!-- one third column -->

                <div class="one-third column">
                    <h2>Responsive Layout</h2><!-- effect 8 -->

                    <section class="set-8">
                        <div class="hi-icon-wrap hi-icon-effect-8">
                            <a href="#" class="hi-icon hi-icon-screen">Responsive Layout</a>
                        </div>
                    </section><!-- effect 8 end -->

                    <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipiscing elit</a>. Sed risus purus, pellentesque a consequat a, accumsan vel libero.</p>
                </div><!-- one third column end -->
                <!-- services column 2 end -->
                <!-- services column 3 -->
                <!-- one third column -->

                <div class="one-third column">
                    <h2>Easy to Customize</h2><!-- effect 8 -->

                    <section class="set-8">
                        <div class="hi-icon-wrap hi-icon-effect-8">
                            <a href="#" class="hi-icon hi-icon-cog">Easy to Customize</a>
                        </div>
                    </section><!-- effect 8 end -->

                    <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipiscing elit</a>. Sed risus purus, pellentesque a consequat a, accumsan vel libero.</p>
                </div><!-- one third column end -->
                <!-- services column 3 end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider left bottom -->

                    <div class="divider-left-bottom"></div><!-- divider left bottom end -->
                </div><!-- sixteen columns end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider right bottom -->

                    <div class="divider-right-bottom"></div><!-- divider right bottom end -->
                </div><!-- sixteen columns end -->
            </div><!-- container end -->
            <!-- sixteen columns -->

            <div class="sixteen columns">
                <!-- divider fin -->

                <div class="divider-fin"></div><!-- divider fin end -->
            </div><!-- sixteen columns end -->
        </div><!-- lower content end -->
        <!-- closer -->

        <div class="fire-closer">
            <a id="fire-services-closer" class="menu-state" href="#"><img src="images/fire-closer.png" alt="Closer"></a>
        </div><!-- closer end -->
        <!-- sixteen columns -->

        <div class="sixteen columns">
            <!-- divider fin -->

            <div class="divider-fin"></div><!-- divider fin end -->
        </div><!-- sixteen columns end -->
    </div><!-- services end -->
    <!-- contact -->

    <div id="contact" class="lower-page">
        <!-- lower content -->

        <div class="lower-content">
            <!-- contact intro -->

            <div class="contact-intro">
                <h1>Contact</h1>
            </div><!-- contact intro end -->
            <!-- container -->

            <div class="container center">
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider left top -->

                    <div class="divider-left-top"></div><!-- divider left top end -->
                </div><!-- sixteen columns end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider right top -->

                    <div class="divider-right-top"></div><!-- divider right top end -->
                </div><!-- sixteen columns end -->
                <!-- contact column 1 -->
                <!-- one third column -->

                <div class="one-third column">
                    <div class="info-address">
                        <p><strong>Address</strong></p>

                        <p>Ash Alum Chemicals<br>
                        28/2 Nehru Nagar West, Bhilai<br>
                        Chhattisgarh</p>
                    </div>
                </div><!-- one third column end -->
                <!-- contact column 1 end -->
                <!-- contact column 2 -->
                <!-- one third column -->

                <div class="one-third column">
                    <div class="info-phone">
                        <p><strong>Telephone</strong></p>

                        <p>+91-9329026463<br>
                        +91-9424114501</p>
                    </div>
                </div><!-- one third column end -->
                <!-- contact column 2 end -->
                <!-- contact column 3 -->
                <!-- one third column -->

                <div class="one-third column">
                    <div class="info-email">
                        <p><strong>Email</strong></p>

                        <p><a href="mailto:ashalum@hotmail.com">ashalum@hotmail.com</a></p>
                    </div>
                </div><!-- one third column end -->
                <!-- contact column 3 end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider blank -->

                    <div class="divider-blank"></div><!-- divider blank end -->
                </div><!-- sixteen columns end -->
                <!-- twitter full -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- twitter ticker -->

                    <div id="twitter-wrap">
                        <!-- twitter logo -->

                        <div id="twitter-logo">
                            <a class="logo" href="https://twitter.com/adityapurandare" target="_blank">Twitter</a>
                        </div><!-- twitter logo end -->

                        <div id="tweets">
                            <div id="ticker" class="query"></div>
                        </div>
                    </div><!-- twitter ticker end -->
                </div><!-- sixteen columns end -->
                <!-- twitter full end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider blank -->

                    <div class="divider-blank"></div><!-- divider blank end -->
                </div><!-- sixteen columns end -->
                <!-- contact form -->

                <div id="form-wrapper">
                    <form id="form" method="post" action="index.php">
                        <div class="holder">
                            <div>
                                <label>NAME *</label> <input id="name" name="name" type="text" class="requiredField name">
                            </div>

                            <div>
                                <label>EMAIL *</label> <input id="email" name="email" type="text" class="requiredField email">
                            </div>

                            <div>
                                <label>SUBJECT *</label> <select name="subject" class="requiredField subject">
                                    <option value="" selected="selected">
                                        Choose a Subject
                                    </option>

                                    <option value="Personal Inquiry">
                                        Personal Inquiry
                                    </option>

                                    <option value="Business Inquiry">
                                        Business Inquiry
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="holder">
                            <div>
                                <label>MESSAGE</label> 
                                <textarea name="message" rows="6" cols="60">
</textarea>
                            </div>

                            <div>
                                <input class="submit" type="submit" value="Submit">
                            </div>
                        </div>
                    </form>

                    <div class="clear"></div>
                </div><!-- contact form end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider left bottom -->

                    <div class="divider-left-bottom"></div><!-- divider left bottom end -->
                </div><!-- sixteen columns end -->
                <!-- sixteen columns -->

                <div class="sixteen columns">
                    <!-- divider right bottom -->

                    <div class="divider-right-bottom"></div><!-- divider right bottom end -->
                </div><!-- sixteen columns end -->
            </div><!-- container end -->
            <!-- sixteen columns -->

            <div class="sixteen columns">
                <!-- divider fin -->

                <div class="divider-fin"></div><!-- divider fin end -->
            </div><!-- sixteen columns end -->
        </div><!-- lower content end -->
        <!-- closer -->

        <div class="fire-closer">
            <a id="fire-contact-closer" class="menu-state" href="#"><img src="images/fire-closer.png" alt="Closer"></a>
        </div><!-- closer end -->
        <!-- sixteen columns -->

        <div class="sixteen columns">
            <!-- divider fin -->

            <div class="divider-fin"></div><!-- divider fin end -->
        </div><!-- sixteen columns end -->
    </div><!-- contact end -->
    <!-- jobs -->
<div id="about" class="lower-page">
<!-- lower content -->
<div class="lower-content">

<!-- jobs intro -->
<div class="about-intro">
<h1>Jobs</h1>
</div>
<!-- about intro end -->

<!-- container -->
<div class="container center">

<!-- sixteen columns -->
<div class="sixteen columns">
<!-- divider left top -->
<div class="divider-left-top"></div>
<!-- divider left top end -->
</div>
<!-- sixteen columns end -->

<!-- sixteen columns -->
<div class="sixteen columns">
<!-- divider right top -->
<div class="divider-right-top"></div>
<!-- divider right top end -->
</div>
<!-- sixteen columns end -->

<!-- about full -->
<!-- sixteen columns -->
<div class="sixteen columns">
<p>Established in 2009, Ash Alum Chemicals has manufactured & delivered products of the highest quality to its customers at most competetive market prices. We manufacture Whitecoal Briquette and are Central India Best Manufacturer and Exporter of Biomass Briquettes maintaining High Consicteny and Customer Satisfaction. Recently awarded as <a href="#">Chattisgarh State's Greenest Maunfacturing Unit Award</a>.<br/><br/>
Led by the Visionary Partner Mr. Ashish Purandare, we are on a Journey to establish Best of Supply Networks around the Globe. 
</p>
</div>
<!-- sixteen columns end -->
<!-- jobs full end -->
    <!-- scripts -->
    <script type="text/javascript" src="js/jquery-1.10.2.min.js">
</script><script type="text/javascript" src="js/jquery.easing.1.3.js">
</script><script type="text/javascript" src="js/supersized.3.2.7.min.js">
</script><script type="text/javascript" src="js/supersized.3.2.7.bg.js">
</script><script type="text/javascript" src="js/supersized.shutter.min.js">
</script><script type="text/javascript" src="js/jquery.nicescroll.3.5.4.js">
</script><script type="text/javascript" src="js/time.js">
</script><script type="text/javascript" src="twitter/jquery.tweet.js">
</script><script type="text/javascript" src="twitter/ticker.js">
</script><script type="text/javascript" src="js/form-subscribe.js">
</script><script type="text/javascript" src="js/form-contact.js">
</script><script type="text/javascript" src="js/countdown.js">
</script><!-- countdown -->
    <script type="text/javascript">
//<![CDATA[
        $(document).ready(function(){
        $("#countdown").countdown({
            date: "01 April 2014 12:00:00", // countdown target date settings
            format: "on"
        },
            
        function() {
            // callback function
        });
      });
    //]]>
    </script><script type="text/javascript" src="js/analytics.js">
</script><!-- scripts end -->
</body>
</html>
