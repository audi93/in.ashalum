<!DOCTYPE html>
<html lang="en">
  <head profile="http://dublincore.org/documents/2008/08/04/dc-html/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Ash Alum Chemicals | Manufacturing Value</title>
	
	<title>Ash Alum Chemicals | Manufacturing Value</title>
    <meta name="description" content="Ash Alum Chemicals is India's One of the Best Manufacturer and Exporter of White Coal Briquettes and other Biomass Products">
    <meta name="author" content="Ashish Purandare">
    <meta name="keywords" content="Ash Alum,Ash, Aluminimum, Alum, Ash Alum Chemicals, AAC, White Coal, Biomass Briquette, White Coal Briquettes, Wood Pellets, ashalumchemicals Bhilai">
	
	<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.Title" content="Ash Alum Chemicals | Manufacturing Value">
    <meta name="DC.Creator" content="Ashish Purandare">
    <meta name="DC.Subject" content="White Coal Briquette Manufacturer">
    <meta name="DC.Description" content="Ash Alum Chemicals is India's One of the Best Manufacturer and Exporter of White Coal Briquettes and other Biomass Products">
    <meta name="DC.Publisher" content="Ash Alum Chemicals">
    <meta name="DC.Contributor" content="Aditya Purandare">
    <meta name="DC.Date" content="2013-03-03">
    <meta name="DC.Type" content="Text">
    <meta name="DC.Language" content="en">
    <meta name="DC.Relation" content="White Coal Briquette">
    <meta name="DC.Coverage" content="India"><!-- Dublin Core Ends Here -->
    <!-- Normailize CSS -->
	<link rel="stylesheet" href="css/normalize.css">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
	<!-- Custom CSS Stylesheets -->
    <style>
    body {
    	background-color:black;
    }
    a,p,h1,h2,h3,h4,h5,h6 {
    	color: white;
    }

	.navbar {
		border-radius: 0;
		border:0;
	} 

	#active a {
		color: white;
	}

	#home {
		color: #39b54a;
	}

	.glyphicon {
		color: #39b54a;
	}

	#nav1toggle {
		border:1px solid #61d76c;
		background-color: #39b54a;
	}

	#nav1toggle:hover {
		background-color: #61d76c;
	}

	.dropdown-menu {
		border-top: 1px solid black;
		background-color: black;
	}

	.dropdown-menu li a {
		color: white;
	}
	.dropdown-menu li a:hover {
		background-color: #39b54a;
		color: white;
	}
	.greenborder {
		height: 3px;
		background-color: #39b54a;
		width: 100%;
	}
	.dropdown:hover .dropdown-menu {
    display: block;
 	}
 	#active a:after {
 		color: #39b54a;
		position: absolute;
		bottom: 0;
		left: 50%;
		width: 0;
		height: 0;
		margin-left: -5px;
		vertical-align: middle;
		content: " ";
		border-right: 5px solid transparent;
		border-bottom: 5px solid;
		border-left: 5px solid transparent;
	}
	#content .row {
		background-color: white;
		height: auto;
		padding-bottom: 50px;
		padding-top:50px;
	}
	#footer-bottom {
		background-color: #181818;
	}
	#footer-top {
		background-color: #252525;
		padding: 0;
		margin: 0;
		border:0;
		width: 100%;
	}
    </style>

	<!-- Social Icons CSS -->
	<link rel="stylesheet" href="css/bootstrap-social.css">
	<link rel="stylesheet" href="css/font-awesome.css">

  </head>
  <body>

	<div class="navbar navbar-inverse navbarcolor">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        			<span class="sr-only">Toggle navigation</span>
        			<span class="icon-bar"></span>
       		 		<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
     			</button>
    		</div>
    		<!-- Collect the nav links, forms, and other content for toggling -->
    		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      			<ul class="nav navbar-nav">
        			<li id="active"><a href="#"><span class="glyphicon glyphicon-home" id="home"></span> Home</a></li>
        			<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <span class=" glyphicon glyphicon-chevron-down"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Achievements</a></li>
							<li><a href="#">Photo Gallery</a></li>
						</ul>
        			</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sister Concerns <span class=" glyphicon glyphicon-chevron-down"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Papusha Gases Pvt. Ltd.</a></li>
							<li><a href="#">Papusha World</a></li>
							<li><a href="#">AVAC Business India</a></li>
							<li><a href="#">Sunenviro Solutions</a></li>
							<li><a href="#">Ashish Weigh Bridge</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Publications <span class=" glyphicon glyphicon-chevron-down"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">News & Events</a></li>
							<li><a href="#">Government Policy</a></li>
						</ul>
					</li>
					<li><a href="#">Contact Us</a></li>
					<!--<li><a href="#">FAQs</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Contact Us <span class=" glyphicon glyphicon-chevron-down"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Staff</a></li>
							<li><a href="#">User Survey</a></li>
						</ul>
					</li>-->
			</div>
		</div>
	</div>
	<div class="navbar navbar-inverse" role="navigation" style="background-color: #39b54a; height: auto; color: white; margin-top: -20px; border:0;">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3" id="nav1toggle">
        			<span class="sr-only">Toggle navigation</span>
        			<span class="icon-bar"></span>
       		 		<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
     			</button>
    		</div>
    		<!-- Collect the nav links, forms, and other content for toggling -->
    		<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-3" style="border:0">
    		    <ul class="nav navbar-nav">
    				<li><a href="mailto:director@ashalum.in" style="color:white;">E-mail: <span style="text-decoration:underline;">director@ashalum.in</span></a></li>
    				<li><a href="#" style="color:white;">Phone: +91-9329-026463</a></li>
    				<li><a href="#" style="color:white;">Login</a></li>
    				<li><a href="#" style="color:white;">Register</a></li>
    			</ul>
				<form class="navbar-form navbar-right" role="search" style="border:0" action="search.php" method="post">
        			<div class="form-group">
          				<input type="text" class="form-control" placeholder="Search" name="search">
        			</div>
        			<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></button>
      			</form>
    		</div>
		</div>
	</div>

	<div class="container-fluid" style="margin-top:-20px;">
		<div class="row">
			<div class="col-xs-9 col-md-6 col-sm-6" style="height:500px;background-color:white;">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3719.905441170536!2d81.19748299999999!3d21.195915!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a293f07dcacd1e1%3A0xbb180f615ad3b9dc!2sAsh+Alum+Chemicals!5e0!3m2!1sen!2sin!4v1398580484904" width="101%" padding="0" margin="0" frameborder="0" style="border:0;" height="100%"></iframe>
			</div>
		</div>
	</div>
	<div class="container-fluid greenborder"></div>
	
	<div class="container-fluid">
		<div id="content">
			<div class="row">
				Content
			</div>
		</div>
	</div>
	<div class="container-fluid greenborder"></div>

	<div id="footer">
		<div class="container-fluid" id="footer-top" style="padding-bottom:10px;">
				<div class="row">
					<h1 class="text-center" style="word-wrap:break-word;">We are coming back online very soon.</h1>
				</div>
			</div>
			<div class="container-fluid" id="footer-bottom" style="padding-top:10px;">
				<div class="row">
					<p class="text-center" style="font-size:12px;margin-bottom:15px;word-wrap:break-word;">&copy; 2014 Ash Alum Chemicals. All Right reserved</p>
					<div style="width:5%; height:1px;background-color:#696969;margin:0 auto;"></div>
					<center>
						<div id="social" style="margin-top:15px;">
							<a class="btn btn-social-icon btn-facebook">
  								<i class="fa fa-facebook"></i>
							</a>
							<a class="btn btn-social-icon btn-twitter">
  								<i class="fa fa-twitter"></i>
							</a>
							<a class="btn btn-social-icon btn-linkedin">
  								<i class="fa fa-linkedin"></i>
  							</a>
  							<a class="btn btn-social-icon btn-google-plus">
  								<i class="fa fa-google-plus"></i>
							</a>
						</div>
					</center>
				</div>
			</div>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>