<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="refresh" content="5;URL='index.php'" />
    <title>Ash Alum Chemicals | Manufacturing Value</title>

    <!-- Normailize CSS -->
	<link rel="stylesheet" href="css/normalize.css">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
	<!-- Custom CSS Stylesheets -->
    <style>
    body {
    	background-color:black;
    }
    a,p,h1,h2,h3,h4,h5,h6 {
    	color: white;
    }

	.navbar {
		border-radius: 0;
	} 

	#active a {
		color: white;
	}

	#home {
		color: #39b54a;
	}

	.glyphicon {
		color: #39b54a;
	}

	#nav1toggle {
		border:1px solid #61d76c;
		background-color: #39b54a;
	}

	#nav1toggle:hover {
		background-color: #61d76c;
	}

	.dropdown-menu {
		background-color: black;
	}

	.dropdown-menu li a {
		color: white;
	}
	.dropdown-menu li a:hover {
		background-color: #39b54a;
		color: white;
	}
	.greenborder {
		height: 3px;
		background-color: #39b54a;
		width: 100%;
	}
    </style>

  </head>
  <body>

	<div class="navbar navbar-inverse navbarcolor">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        			<span class="sr-only">Toggle navigation</span>
        			<span class="icon-bar"></span>
       		 		<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
     			</button>
    		</div>
    		<!-- Collect the nav links, forms, and other content for toggling -->
    		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      			<ul class="nav navbar-nav">
        			<li id="active"><a href="#"><span class="glyphicon glyphicon-home" id="home"></span> Home</a></li>
        			<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <span class=" glyphicon glyphicon-chevron-down"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Achievements</a></li>
							<li><a href="#">Photo Gallery</a></li>
						</ul>
        			</li>
        			<li><a href="#">Bio-Vision</a></li>
        			<li><a href="#">Support Price</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Government Policy <span class=" glyphicon glyphicon-chevron-down"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Resolution</a></li>
							<li><a href="#">Notification</a></li>
							<li><a href="#">Project</a></li>
							<li><a href="#">Biodiesel Purchase Policy</a></li>
							<li><a href="#">Benefits to Farmers</a></li>
							<li><a href="#">Right to Information</a></li>
						</ul>
					</li>
					<li><a href="#">Biodiesel Plant</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Publications <span class=" glyphicon glyphicon-chevron-down"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Press Releases</a></li>
						</ul>
					</li>
					<li><a href="#">FAQs</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Contact Us <span class=" glyphicon glyphicon-chevron-down"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Staff</a></li>
							<li><a href="#">User Survey</a></li>
						</ul>
					</li>
			</div>
		</div>
	</div>
	<div class="navbar navbar-inverse" role="navigation" style="background-color: #39b54a; height: auto; color: white; margin-top: -20px; border:0;">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3" id="nav1toggle">
        			<span class="sr-only">Toggle navigation</span>
        			<span class="icon-bar"></span>
       		 		<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
     			</button>
    		</div>
    		<!-- Collect the nav links, forms, and other content for toggling -->
    		<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-3" style="border:0">
    		    <ul class="nav navbar-nav">
    				<li><a href="mailto:director@ashalum.in" style="color:white;">E-mail: <span style="text-decoration:underline;">director@ashalum.in</span></a></li>
    				<li><a href="#" style="color:white;">Phone: +91-9329-026463</a></li>
    				<li><a href="#" style="color:white;">Login</a></li>
    				<li><a href="#" style="color:white;">Register</a></li>
    			</ul>
				<form class="navbar-form navbar-right" role="search" style="border:0">
        			<div class="form-group">
          				<input type="text" class="form-control" placeholder="Search" name="search">
        			</div>
        			<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"</button>
      			</form>
    		</div>
		</div>
	</div>

	<div class="container-fluid" style="height:500px; margin-top:-20px;margin-left:-20px;">
		<h1 class="text-center" style="color:#d43f3a">Oops...</h1>
		<h2 class="text-center">Looks like the page you were looking for doesn't exist. Sorry about that.</h2>
		<p class="text-center">Redirecting you to Home Page in 5 seconds.</p>
	</div>
	<div class="container-fluid greenborder"></div>

	<div id="footer">
		<div class="container-fluid">
			<h1 class="text-center">We are coming back online very soon.</h1>
			<p class="text-center">&copy; 2014 Ash Alum Chemicals, All Right reserved</p>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>